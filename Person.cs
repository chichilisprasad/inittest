﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPS
{
    public class Person
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public virtual void Display()
        {
            Console.WriteLine("I am in person class");
        }
        public void print()
        {
            Console.WriteLine("I am Testting pull");
        }
    }
    public class Employee : Person
    {
        public int Salary { get; set; }
        public new void print()
        {
            Console.WriteLine("I am in Employee class");
        }
        public override void Display()
        {
            Console.WriteLine("I am in Employee class");
        }
    }
}
